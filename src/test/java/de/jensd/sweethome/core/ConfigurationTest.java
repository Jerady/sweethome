package de.jensd.sweethome.core;

import org.junit.Test;

/**
 *
 * @author Jens Deters
 */
public class ConfigurationTest {

    @Test
    public void readAndWriteConfiguration() {
        Configuration configuration = Configuration.load();

        System.out.println("\n\n Configuration loaded");

        System.out.println(configuration.getAtHome());
        System.out.println(configuration.getServerName());
        System.out.println(configuration.getLanServer());
        System.out.println(configuration.getWanServer());
        System.out.println(configuration.getHttpPort());
        System.out.println(configuration.getHttpContext());

        configuration.setAtHome(!configuration.getAtHome());
        System.out.println("\n\n Configuration changed");
        configuration.save();

        configuration = Configuration.load();
        System.out.println("\n\n Configuration reloaded");

        System.out.println(configuration.getAtHome());
        System.out.println(configuration.getServerName());
        System.out.println(configuration.getLanServer());
        System.out.println(configuration.getWanServer());
        System.out.println(configuration.getHttpPort());
        System.out.println(configuration.getHttpContext());


    }
}
