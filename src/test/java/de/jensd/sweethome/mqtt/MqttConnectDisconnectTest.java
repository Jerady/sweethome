/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.jensd.sweethome.mqtt;

import de.jensd.sweethome.core.MqttClientConfiguration;
import de.jensd.sweethome.core.MqttClientController;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author jdeters
 */
public class MqttConnectDisconnectTest {

    private MqttClientConfiguration configuration;

    public MqttConnectDisconnectTest() {

    }

    @Before
    public void initConfiguration() {
        configuration = new MqttClientConfiguration();
    }

    @Test
    public void connectDisonnect() throws MqttException {

        MqttClientController clientController = new MqttClientController(configuration);
        clientController.connect();
        clientController.disconnect();
    }
}
