/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.jensd.sweethome.mqtt;

import de.jensd.sweethome.core.MqttClientConfiguration;
import de.jensd.sweethome.core.MqttClientController;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.junit.Before;
import org.junit.Test;
import org.vertx.java.core.json.JsonObject;

/**
 *
 * @author jdeters
 */
public class MqttPublishTest {

    private MqttClientConfiguration configuration;
    private MqttClientController clientController;

    public MqttPublishTest() {

    }

    @Before
    public void init() throws MqttException {
        configuration = new MqttClientConfiguration();
        clientController = new MqttClientController(configuration);
        clientController.connect();
    }

    @Test
    public void publish() throws MqttException {
        if (clientController.isClientConnected()) {
            String topic = "/SweetHome/Test";
            JsonObject jsonObject = new JsonObject();
            jsonObject.putString("name", "Springbrunnen");
            jsonObject.putString("houseCode", "1");
            jsonObject.putString("group", "1");
            jsonObject.putString("device", "1");
            jsonObject.putString("command", "1");

            byte[] payload = jsonObject.encode().getBytes();
            MqttMessage message = new MqttMessage(payload);
            clientController.getMqttClient().publish(topic, message);
        }

    }
}
