/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.jensd.sweethome.server;

import java.io.IOException;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author jdeters
 */
public class RESTTest {

    public static void main(String[] args) throws IOException {

        String response = ClientBuilder
                .newClient()
                .target("http://heubaum.synology.me:14880")
                .path("/intertechno/send/a/1/3/1")
                .request(MediaType.TEXT_PLAIN_TYPE)
                .get(String.class);
        
        System.out.println(response);
    }

}
