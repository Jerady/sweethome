package de.jensd.sweethome.server;

import de.jensd.sweethome.core.HttpDeviceControl;
import de.jensd.sweethome.core.Configuration;
import de.jensd.sweethome.core.Device;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Jens Deters
 */
public class SendHttpCommand {

    @Test
    public void sendHttpCommand() {
        Configuration configuration = new Configuration();
        HttpDeviceControl httpCommand = new HttpDeviceControl(configuration);
        Device terrace = new Device("Terrace", "a", "1", "2");
        String result = httpCommand.turnOn(terrace);
        assertEquals("ok", result.toLowerCase());        
        System.out.println(result);
    }
}
