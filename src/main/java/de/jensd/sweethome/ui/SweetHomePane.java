/**
 * Copyright (c) 2013, Jens Deters
 * http://www.jensd.de
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package de.jensd.sweethome.ui;

import de.jensd.fx.fontawesome.AwesomeDude;
import de.jensd.fx.fontawesome.AwesomeIcon;
import de.jensd.sweethome.core.Device;
import de.jensd.sweethome.core.Configuration;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ToggleButton;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.TouchEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;

/**
 *
 * @author Jens Deters
 */
public class SweetHomePane extends AnchorPane {

    @FXML
    private Button exitButton;
    @FXML
    private ToggleButton locationButton;
    @FXML
    private Label locationLabel;
    @FXML
    private VBox devicesPane;
    private Configuration configuration;

    public SweetHomePane(final Configuration configuration) {
        this.configuration = configuration;
        init();
    }

    private void init() {

        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/SweetHomePane.fxml"));
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);
        try {
            fxmlLoader.load();
        } catch (IOException ex) {
            Logger.getLogger(DevicePane.class.getName()).log(Level.SEVERE, null, ex);
        }


        // Running on RasPi always provide a way out instead of kill command ;-)
        final EventHandler<KeyEvent> exitOnCrtlCEventHandler =
                new EventHandler<KeyEvent>() {
            @Override
            public void handle(final KeyEvent keyEvent) {
                if (keyEvent.isControlDown() && KeyCode.C.equals(keyEvent.getCode())) {
                    Platform.exit();
                    keyEvent.consume();
                }
            }
        };
        setOnKeyPressed(exitOnCrtlCEventHandler);


        final EventHandler<TouchEvent> exitOnTouch = new EventHandler<TouchEvent>() {
            @Override
            public void handle(TouchEvent t) {
                Platform.exit();
            }
        };

        exitButton.setOnTouchPressed(exitOnTouch);
        AwesomeDude.setIcon(exitButton, AwesomeIcon.POWER_OFF, "48px");
        exitButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent t) {
                exit();
            }
        });

        locationButton.selectedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> ov, Boolean oldValue, Boolean newValue) {
                if (newValue) {
                    setLocationLAN();
                } else {
                    setLocationWAN();
                }
            }
        });
        locationButton.setSelected(configuration.isAtHome());

        if (configuration.isAtHome()) {
            setLocationLAN();
        } else {
            setLocationWAN();
        }

        setOnTouchReleased(exitOnTouch);

        List<Device> devices = configuration.getDevices();
        for (Device device : devices) {
            addDevice(device);
        }
    }

    public void addDevice(Device device) {
        DevicePane devicePane = new DevicePane(device, configuration);
        devicesPane.getChildren().add(devicePane);
    }

    private void setLocationLAN() {
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                AwesomeDude.setIcon(locationButton, AwesomeIcon.HOME, "48px");
                locationLabel.setText(configuration.getLanServer());
                configuration.setAtHome(Boolean.TRUE);
            }
        });
    }

    private void setLocationWAN() {
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                AwesomeDude.setIcon(locationButton, AwesomeIcon.CLOUD, "48px");
                locationLabel.setText(configuration.getWanServer());
                configuration.setAtHome(Boolean.FALSE);
            }
        });
    }

    @FXML
    public void exit() {
        configuration.save();
        Platform.exit();
    }
}
