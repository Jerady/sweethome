/**
 * Copyright (c) 2013, Jens Deters
 * http://www.jensd.de
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package de.jensd.sweethome.ui;

import de.jensd.fx.fontawesome.AwesomeDude;
import de.jensd.fx.fontawesome.AwesomeIcon;
import de.jensd.sweethome.core.Configuration;
import de.jensd.sweethome.core.Device;
import de.jensd.sweethome.core.DeviceControl;
import de.jensd.sweethome.core.HttpDeviceControl;
import de.jensd.sweethome.core.RESTDeviceControl;
import de.jensd.sweethome.core.Send;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.layout.AnchorPane;

/**
 *
 * @author Jens Deters
 */
public class DevicePane extends AnchorPane {

    @FXML
    private Button offButton;
    @FXML
    private Button onButton;
    private Device device;
    private DeviceControl deviceControl;
    private HttpDeviceControl httpDeviceControl;
    private RESTDeviceControl restDeviceControl;
    private Configuration configuration;
    private Service<String> turnOnService;
    private Service<String> turnOffService;

    public DevicePane(final Device device, final Configuration configuration) {
        this.configuration = configuration;
        this.device = device;
        init();
    }

    private void init() {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/DevicePane.fxml"));
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);
        try {
            fxmlLoader.load();
        } catch (IOException ex) {
            Logger.getLogger(DevicePane.class.getName()).log(Level.SEVERE, null, ex);
        }

        httpDeviceControl = new HttpDeviceControl(configuration);
        restDeviceControl = new RESTDeviceControl(configuration);
        deviceControl = new DeviceControl();

        AwesomeDude.setIcon(onButton, AwesomeIcon.LIGHTBULB_ALT, "72px");
        AwesomeDude.setIcon(offButton, AwesomeIcon.LIGHTBULB_ALT, "72px");

        turnOnService = new SwitchService(device, Send.Command.TURN_ON);
        turnOffService = new SwitchService(device, Send.Command.TURN_OFF);

        //deviceNameText.setText(device.getName());


        onButton.setText(device.getName());

        offButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent t) {
                turnOffService.restart();
            }
        });

        onButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent t) {
                turnOnService.restart();
            }
        });
    }

    private class SwitchService extends Service<String> {

        private Device device;
        private Send.Command command;

        public SwitchService(Device device, Send.Command command) {
            this.device = device;
            this.command = command;
        }

        @Override
        protected Task<String> createTask() {
            return new Task<String>() {
                @Override
                protected String call() {
                    deviceControl.doSwitch(device, command);
                    return restDeviceControl.doSwitch(device, command);
                }
            };
        }
    }
}
