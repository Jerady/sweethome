/**
 * Copyright (c) 2013, Jens Deters
 * http://www.jensd.de
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package de.jensd.sweethome.ui;

import de.jensd.sweethome.core.Configuration;
import javafx.application.Application;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Stage;

/**
 * @author Jens Deters
 *
 */
public class Run extends Application {

    public final static String APP_FONT_PATH = "/fonts/DroidSans.ttf";

    @Override
    public void start(Stage primarystage) throws Exception {

        Font.loadFont(Run.class.getResource(APP_FONT_PATH).toExternalForm(), 10.0);

        Configuration configuration = Configuration.load();

        Parent root = new SweetHomePane(configuration);

        Scene scene = new Scene(root, 1280, 720, Color.BLACK);
        scene.getStylesheets()
                .addAll("/styles/sweethome.css");
        primarystage.setScene(scene);
        primarystage.show();
    }

    public static void main(String[] args) {
        launch(args);

    }
}
