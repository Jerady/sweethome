/**
 * Copyright (c) 2014, Jens Deters http://www.jensd.de
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *
 */
package de.jensd.sweethome.server.mqtt;

import de.jensd.sweethome.core.Configuration;
import java.util.Scanner;
import org.eclipse.paho.client.mqttv3.MqttException;

/**
 *
 * @author Jens Deters
 */
public class Run {

    public static void main(String[] args) throws MqttException {

        Configuration configuration = Configuration.load();
        MqttMessageReceiver messageReceiver = new MqttMessageReceiver(configuration.getMqttClientConfiguration());
        messageReceiver.start();


        Scanner scan = new Scanner(System.in);
        while (true) {
            String in = scan.next();
            if (in.equalsIgnoreCase("Q")) {
                messageReceiver.stop();
                break;
            }
        }

    }

}
