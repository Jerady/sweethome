/**
 * Copyright (c) 2014, Jens Deters http://www.jensd.de
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *
 */
package de.jensd.sweethome.server.mqtt;

import de.jensd.sweethome.core.MqttClientConfiguration;
import de.jensd.sweethome.core.MqttClientController;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 *
 * @author Jens Deters
 */
public class MqttMessageReceiver implements MqttCallback {

    private final MqttClientController mqttClientController;
    private final MqttClientConfiguration configuration;

    public MqttMessageReceiver(MqttClientConfiguration configuration) {
        mqttClientController = new MqttClientController(configuration);
        this.configuration = configuration;
    }

    public void start() throws MqttException {

        mqttClientController.connect();
        mqttClientController.getMqttClient().setCallback(this);
        mqttClientController.getMqttClient().subscribe(configuration.getMqttMessagesTopic());

        System.out.println("---------------------------------------");
        System.out.println("-                                     -");
        System.out.println("-      S W E E T H O M E  SERVER      -");
        System.out.println("-                                     -");
        System.out.println("---------------------------------------");
        System.out.println();
        System.out.println("---------------------------------------");
        System.out.println("Server started.");
        System.out.println("Enter 'q' to shutdown server.");
        System.out.println("Subscribed to MQTT Topic: " + configuration.getMqttMessagesTopic());
        System.out.println("Waiting for incoming requests...");
    }

    public void stop() throws MqttException {
        System.out.println("Sweethome-Server: shutdown requested");
        mqttClientController.disconnect();
        System.out.println("Sweethome-Server: halted.");
    }

    @Override
    public void connectionLost(Throwable cause) {
    }

    @Override
    public void deliveryComplete(IMqttDeliveryToken token) {
    }

    @Override
    public void messageArrived(String topic, MqttMessage message) throws Exception {
        String payload = new String(message.getPayload());
        decodeMessage(topic, payload);
    }

    private void decodeMessage(final String topic, final String message) {
        try {
            JSONParser parser = new JSONParser();
            Object obj = parser.parse(message);
            JSONObject parsedJsonObject = (JSONObject) obj;
            String name = (String) parsedJsonObject.get("name");
            String houseCode = (String) parsedJsonObject.get("houseCode");
            String group = (String) parsedJsonObject.get("group");
            String device = (String) parsedJsonObject.get("device");
            String command = (String) parsedJsonObject.get("command");
            
            System.out.println("name=" + name);
            System.out.println("houseCode=" + houseCode);
            System.out.println("group=" + group);
            System.out.println("device=" + device);
            System.out.println("command=" + command);
            
            
        } catch (ParseException ex) {
            System.out.println("Can't parse message: " + message);
        }
    }

}
