/**
 * Copyright (c) 2013, Jens Deters
 * http://www.jensd.de
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package de.jensd.sweethome.server.vertx;

import de.jensd.sweethome.core.Send;
import org.vertx.java.core.Handler;
import org.vertx.java.core.http.HttpServer;
import org.vertx.java.core.http.HttpServerRequest;
import org.vertx.java.core.http.RouteMatcher;
import org.vertx.java.platform.Verticle;

/**
 *
 * @author Jens Deters
 */
public class IntertechnoVerticle extends Verticle {

    @Override
    public void start() {
        RouteMatcher routeMatcher = new RouteMatcher();
        routeMatcher.get("/intertechno/send/:houseCode/:group/:device/:command", new Handler<HttpServerRequest>() {
            @Override
            public void handle(HttpServerRequest req) {
                container.logger().info("received request from: " + req.remoteAddress());
                
                String deviceCode = String.format("%s %s %s", req.params().get("houseCode"), req.params().get("group"), req.params().get("device"));
                String commandParam = (String) req.params().get("command");
                Send.Command command = "0".equals(commandParam) ? Send.Command.TURN_OFF : Send.Command.TURN_ON;
                
                container.logger().info("Send: " + deviceCode + " " + command.name());
                new Send().send(deviceCode, command);
                container.logger().info("OK");
                
                req.response().setStatusCode(200);
                req.response().end("<b>Send: " + deviceCode + " " + command.name() + "</b>");
            }
        });

        int port = 14880;
        HttpServer server = vertx.createHttpServer();
        server.requestHandler(routeMatcher)
                .listen(port);

        container.logger().info("---------------------------------------");
        container.logger().info("-                                     -");
        container.logger().info("-      S W E E T H O M E  SERVER      -");
        container.logger().info("-                                     -");
        container.logger().info("---------------------------------------");
        container.logger().info("Server started.");
        container.logger().info("Listening on port " + port);
        container.logger().info("Waiting for incoming requests...");
    }
}