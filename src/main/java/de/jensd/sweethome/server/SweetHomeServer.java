/**
 * Copyright (c) 2013, Jens Deters
 * http://www.jensd.de
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package de.jensd.sweethome.server;

import com.sun.net.httpserver.HttpContext;
import com.sun.net.httpserver.HttpServer;
import java.net.InetSocketAddress;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Jens Deters
 */
public class SweetHomeServer {

    public int httpPort;
    public String httpContext;
    private HttpServer httpServer;

    public SweetHomeServer(final int httpPort, final String httpContext) {
        this.httpPort = httpPort;
        this.httpContext = httpContext;
        init();
    }

    private void init() {
        try {
            httpServer = HttpServer.create(new InetSocketAddress(httpPort), 0);
            HttpContext context = httpServer.createContext("/" + httpContext, new HttpCommandHandler());
            context.getFilters().add(new GenericParameterFilter());
        } catch (Exception ex) {
            Logger.getLogger(SweetHomeServer.class.getName()).log(Level.SEVERE, ex.getMessage());
        }
    }

    public void start() {
        if (httpServer == null) {
            Logger.getLogger(SweetHomeServer.class.getName()).log(Level.SEVERE, "HttpServer was not created due an error.");
            return;
        }
        httpServer.start();
        System.out.println("---------------------------------------");
        System.out.println("-                                     -");
        System.out.println("-      S W E E T H O M E  SERVER      -");
        System.out.println("-                                     -");
        System.out.println("---------------------------------------");
        System.out.println();
        System.out.format("     Context:  %s\n", httpContext);
        System.out.format("     Port:     %d\n", httpPort);
        System.out.println();
        System.out.println("---------------------------------------");
        System.out.println("Server started.");
        System.out.println("Enter 'q' to shutdown server.");
        System.out.println("Waiting for incoming requests...");
    }

    public void stop() {
        if (httpServer == null) {
            Logger.getLogger(SweetHomeServer.class.getName()).log(Level.SEVERE, "HttpServer was not created");
            throw new IllegalStateException("HttpServer was not created");
        }
        System.out.println("Sweethome-Server: shutdown requested");
        httpServer.stop(0);
        System.out.println("Sweethome-Server: halted.");
    }
}
