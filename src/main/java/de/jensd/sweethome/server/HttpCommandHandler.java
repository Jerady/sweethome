/**
 * Copyright (c) 2013, Jens Deters
 * http://www.jensd.de
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package de.jensd.sweethome.server;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import de.jensd.sweethome.core.Send;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Jens Deters
 */
public class HttpCommandHandler implements HttpHandler {

    private static final Logger logger = Logger.getLogger(HttpCommandHandler.class.getName());
    

    @Override
    public void handle(HttpExchange exchange) throws IOException {
        Map<String, Object> params = (Map<String, Object>) exchange.getAttribute("parameters");

        logger.log(Level.INFO, "received params:{0}", params);
        logger.log(Level.INFO, "from :{0}", exchange.getRemoteAddress());

        final String deviceCode = String.format("%s %s %s", params.get("houseCode"), params.get("group"), params.get("device"));
        final String commandParam = (String) params.get("command");
        final Send.Command command = "0".equals(commandParam) ? Send.Command.TURN_OFF : Send.Command.TURN_ON;

        logger.log(Level.INFO, "Send: {0} {1}", new Object[]{deviceCode, command.name()});

        new Send().send(deviceCode, command);
        String response = "OK";
        exchange.sendResponseHeaders(200, response.length());
        try (OutputStream os = exchange.getResponseBody()) {
            os.write(response.getBytes());
        }
        logger.log(Level.INFO, "Response: OK");

    }
}
