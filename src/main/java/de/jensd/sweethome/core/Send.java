/**
 * Copyright (c) 2013, Jens Deters
 * http://www.jensd.de
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package de.jensd.sweethome.core;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Jens Deters
 */
public class Send {

    private static final Logger logger = Logger.getLogger(Send.class.getName());

    public enum Command {
        TURN_OFF, TURN_ON;
    }
    public static final String SEND_COMMAND = "/home/pi/rcswitch-pi/send";

    public String send(String deviveCode, Command command) {
        StringBuilder builder = new StringBuilder();
        try {
            String commandLine = String.
                    format("%s %s %s", SEND_COMMAND, deviveCode, command.ordinal());
            logger.log(Level.INFO, "send: {0}", commandLine);

            Runtime rt = Runtime.getRuntime();
            Process pr = rt.exec(commandLine);
            BufferedReader rd = new BufferedReader(new InputStreamReader(pr.
                    getInputStream()));
            String line;
            while ((line = rd.readLine()) != null) {
                logger.log(Level.INFO, line);
                builder.append(line);
            }

        } catch (IOException ex) {
            Logger.getLogger(Send.class.getName()).
                    log(Level.SEVERE, null, ex);
        } finally {
            return builder.toString();
        }
    }

    public boolean isSendCommandExecutable() {
        return Files.isExecutable(Paths.get(SEND_COMMAND));
    }
}
