/**
 * Copyright (c) 2013, Jens Deters http://www.jensd.de
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *
 */
package de.jensd.sweethome.core;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javax.xml.bind.JAXB;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author Jens Deters
 */
@XmlRootElement
@XmlType(propOrder = {"devices", "atHome", "mqttClientConfiguration", "serverName", "httpPort", "httpContext", "lanServer", "wanServer"})
public class Configuration {

    private static final Logger logger = Logger.getLogger(Configuration.class.getName());
    public static final String CONFIG_FILE_NAME = "sweethome-config.xml";
    private List<Device> devices;
    private StringProperty serverNameProperty;
    private StringProperty httpPortProperty;
    private StringProperty httpContextProperty;
    private StringProperty lanServerProperty;
    private StringProperty wanServerProperty;
    private BooleanProperty atHomeProperty;
    private MqttClientConfiguration mqttClientConfiguration;

    public Configuration() {
        init();
    }

    private void init() {
        serverNameProperty = new SimpleStringProperty();
        httpPortProperty = new SimpleStringProperty();
        httpContextProperty = new SimpleStringProperty();
        lanServerProperty = new SimpleStringProperty();
        wanServerProperty = new SimpleStringProperty();
        atHomeProperty = new SimpleBooleanProperty();

        atHomeProperty.addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> ov, Boolean oldValue, Boolean newValue) {
                if (newValue) {
                    setServerName(lanServerProperty.getValue());
                } else {
                    setServerName(wanServerProperty.getValue());
                }
            }
        });

    }

    public static Configuration load() {

        if (!getConfigFile().exists()) {
            logger.log(Level.INFO, "{0} doesn''t exist. Creating it for you with default settings.", getConfigFile());
            createAndSaveDefaultConfig();
        }

        logger.log(Level.INFO, "Loading configuration: {0}", getConfigFile());

        Configuration configuration = JAXB.unmarshal(getConfigFile(), Configuration.class);

        if (configuration.isAtHome()) {
            configuration.setServerName(configuration.getLanServer());
        } else {
            configuration.setServerName(configuration.getWanServer());
        }
        return configuration;
    }

    public void save() {
        JAXB.marshal(this, getConfigFile());
    }

    private static void createAndSaveDefaultConfig() {
        Configuration configuration = new Configuration();
        configuration.setHttpPort("14880");
        configuration.setHttpContext("sweethome");
        configuration.setServerName("192.168.0.61");
        configuration.setLanServer("192.168.0.61");
        configuration.setWanServer("heubaum.synology.me");
        configuration.setAtHome(Boolean.TRUE);
        List<Device> devices = new ArrayList<>();
        devices.add(new Device("Licht Haustür", "a", "1", "1"));
        devices.add(new Device("Licht Terrasse", "a", "1", "2"));
        devices.add(new Device("Springbrunnen", "a", "1", "3"));
      //  devices.add(new Device("Licht Gartenhaus", "a", "1", "4"));
        MqttClientConfiguration mqttClientConfiguration = new MqttClientConfiguration();
        configuration.setMqttClientConfiguration(mqttClientConfiguration);
        configuration.setDevices(devices);
        configuration.save();
    }

    public static File getConfigFile() {
        return new File(System.getProperty("user.home") + "/" + CONFIG_FILE_NAME);
    }

    public MqttClientConfiguration getMqttClientConfiguration() {
        return mqttClientConfiguration;
    }

    public void setMqttClientConfiguration(MqttClientConfiguration mqttClientConfiguration) {
        this.mqttClientConfiguration = mqttClientConfiguration;
    }

    @XmlElementWrapper
    @XmlElement(name = "device")
    public List<Device> getDevices() {
        return devices;
    }

    public void setDevices(List<Device> devices) {
        this.devices = devices;
    }

    public StringProperty serverNameProperty() {
        return serverNameProperty;
    }

    public StringProperty httpPortProperty() {
        return httpPortProperty;
    }

    public StringProperty httpContextProperty() {
        return httpContextProperty;
    }

    public StringProperty lanServerProperty() {
        return lanServerProperty;
    }

    public StringProperty wanServerProperty() {
        return wanServerProperty;
    }

    public BooleanProperty atHomeProperty() {
        return atHomeProperty;
    }

    public String getServerName() {
        return serverNameProperty.getValue();
    }

    public void setServerName(String serverName) {
        this.serverNameProperty.setValue(serverName);
    }

    public String getHttpPort() {
        return httpPortProperty.getValue();
    }

    public void setHttpPort(String httpPort) {
        this.httpPortProperty.setValue(httpPort);
    }

    public String getHttpContext() {
        return httpContextProperty.getValue();
    }

    public void setHttpContext(String httpContext) {
        this.httpContextProperty.setValue(httpContext);
    }

    public String getLanServer() {
        return lanServerProperty.getValue();
    }

    public void setLanServer(String lanIp) {
        this.lanServerProperty.setValue(lanIp);
    }

    public String getWanServer() {
        return wanServerProperty.getValue();
    }

    public void setWanServer(String wanIp) {
        this.wanServerProperty.setValue(wanIp);
    }

    public boolean isAtHome() {
        return atHomeProperty.getValue();
    }

    public Boolean getAtHome() {
        return atHomeProperty.getValue();
    }

    public void setAtHome(Boolean atHome) {
        if (atHome) {
            setServerName(getLanServer());
        } else {
            setServerName(getWanServer());
        }
        this.atHomeProperty.setValue(atHome);
    }
}
