/**
 * Copyright (c) 2013, Jens Deters
 * http://www.jensd.de
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package de.jensd.sweethome.core;

/**
 *
 * @author Jens Deters
 */
public class Device {

  private String name;
  private String houseCode;
  private String groupId;
  private String deviceId;

  public Device() {
  }

  public Device(String name, String houseCode, String group, String device) {
    this.name = name;
    this.houseCode = houseCode;
    this.groupId = group;
    this.deviceId = device;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getDeviceId() {
    return deviceId;
  }

  public void setDeviceId(String deviceId) {
    this.deviceId = deviceId;
  }

  public String getGroupId() {
    return groupId;
  }

  public void setGroupId(String groupId) {
    this.groupId = groupId;
  }

  public String getHouseCode() {
    return houseCode;
  }

  public void setHouseCode(String houseCode) {
    this.houseCode = houseCode;
  }

  public String getId() {
    return String.format("%s %s %s", houseCode, groupId, deviceId);
  }
}
