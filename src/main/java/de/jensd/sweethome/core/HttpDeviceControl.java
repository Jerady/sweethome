/**
 * Copyright (c) 2013, Jens Deters
 * http://www.jensd.de
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package de.jensd.sweethome.core;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;

/**
 *
 * @author Jens Deters
 *
 */
public class HttpDeviceControl extends DeviceControl{

    private static final Logger logger = Logger.getLogger(HttpDeviceControl.class.getName());
    private URL url;
    private Configuration configuration;

    public HttpDeviceControl(Configuration configuration) {
        this.configuration = configuration;
        init();
    }

    private void init() {
        initServerURL();
        configuration.serverNameProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> ov, String t, String t1) {
                initServerURL();
            }
        });

    }

    private void initServerURL() {
        String urlString = String.format("http://%s:%s/%s", configuration.getServerName(), configuration.getHttpPort(), configuration.getHttpContext());
        logger.log(Level.INFO, "Server URL changed: {0}", urlString);
        try {
            url = new URL(urlString);
        } catch (MalformedURLException ex) {
            logger.log(Level.SEVERE, ex.getMessage());
        }
    }

    public String createRequestParams(String houseCode, String group, String device, String command) throws MalformedURLException, UnsupportedEncodingException {
        String params =
                "houseCode=" + URLEncoder.encode(houseCode, "UTF-8") + "&"
                + "group=" + URLEncoder.encode(group, "UTF-8") + "&"
                + "device=" + URLEncoder.encode(device, "UTF-8") + "&"
                + "command=" + URLEncoder.encode(command, "UTF-8");
        return params;
    }

    @Override
    public String doSwitch(Device device, Send.Command command) {
        logger.log(Level.INFO, "doSwitch: about to {0} {1} ({2})", new Object[]{command, device.getName(), device.getId()});
        return send(device.getHouseCode(), device.getGroupId(), device.getDeviceId(), String.valueOf(command.ordinal()));
    }
    
    public String send(String houseCode, String group, String device, String command) {
        StringBuilder result = new StringBuilder();
        try {
            String requestParams = createRequestParams(houseCode, group, device, command);

            logger.log(Level.INFO, "{0}?{1}", new Object[]{url.toExternalForm(), requestParams});

            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("POST");
            connection.setDoInput(true);
            connection.setDoOutput(true);
            connection.setUseCaches(false);
            connection.setRequestProperty("Content-Type",
                    "application/x-www-form-urlencoded");
            connection.setRequestProperty("Content-Length", String.valueOf(requestParams.length()));


            BufferedReader reader;
            try (OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream())) {
                writer.write(requestParams);
                writer.flush();
                reader = new BufferedReader(
                        new InputStreamReader(connection.getInputStream()));
                for (String line; (line = reader.readLine()) != null;) {
                    result.append(line);
                }
            }
            reader.close();

        } catch (Exception ex) {
            logger.log(Level.SEVERE, ex.getLocalizedMessage());
        } finally {
            return result.toString();
        }


    }
}
