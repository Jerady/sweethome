/**
 * Copyright (c) 2014, Jens Deters http://www.jensd.de
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *
 */
package de.jensd.sweethome.core;

import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttClientPersistence;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;

/**
 *
 * @author Jens Deters
 */
public class MqttClientController {

    private final static Logger LOGGER = Logger.getLogger(MqttClientController.class.getName());
    private StringProperty messageProperty;
    private BooleanProperty clientConnectedProperty;
    private ObjectProperty<MqttClient> mqttClientProperty;
    private final MqttClientConfiguration mqttClientConfiguration;
    private MqttClientPersistence persistence;

    public MqttClientController(MqttClientConfiguration mqttClientConfiguration) {
        this.mqttClientConfiguration = mqttClientConfiguration;
    }

    public MqttClientPersistence getPersistence() {
        if (persistence == null) {
            persistence = new MemoryPersistence();
        }
        return persistence;
    }

    public StringProperty messageProperty() {
        if (messageProperty == null) {
            messageProperty = new SimpleStringProperty();
        }
        return messageProperty;
    }

    public void setMessage(String message) {
        messageProperty().set(message);
    }

    public String getMessage() {
        return messageProperty().get();
    }

    public ObjectProperty<MqttClient> mqttClientProperty() {
        if (mqttClientProperty == null) {
            mqttClientProperty = new SimpleObjectProperty<>();
        }
        return mqttClientProperty;
    }

    public void setMqttClient(MqttClient mqttClient) {
        mqttClientProperty().set(mqttClient);
    }

    public MqttClient getMqttClient() {
        return mqttClientProperty().get();
    }

    public BooleanProperty clientConnectedProperty() {
        if (clientConnectedProperty == null) {
            clientConnectedProperty = new SimpleBooleanProperty(Boolean.FALSE);
        }
        return clientConnectedProperty;
    }

    public void setClientConnected(Boolean clientConnected) {
        clientConnectedProperty().set(clientConnected);
    }

    public boolean isClientConnected() {
        if (getMqttClient() != null) {
            return getMqttClient().isConnected();
        }
        return false;
    }

    public void connect() throws MqttException {
        if (getMqttClient() == null || !getMqttClient().isConnected()) {
            LOGGER.info("connect");
            setMessage("connect");
            String clientID = mqttClientConfiguration.getMqttClientId();
            String brokerURI = mqttClientConfiguration.getMqttBrokerURI();
            LOGGER.log(Level.INFO, "connecting to: {0} with clientID: {1}", new Object[]{brokerURI, clientID});
            MqttClient mqttClient = new MqttClient(brokerURI, clientID, persistence);
            mqttClient.connect();
            setMqttClient(mqttClient);
            setClientConnected(Boolean.TRUE);
            setMessage("connected");
        }
    }

    public void disconnect() throws MqttException {
        if (getMqttClient() != null && getMqttClient().isConnected()) {
            LOGGER.info("disconnect");
            setMessage("disconnect");
            getMqttClient().disconnect();
            setClientConnected(Boolean.FALSE);
            setMessage("disconnected");
        }
    }
}
