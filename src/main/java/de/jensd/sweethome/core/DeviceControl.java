/**
 * Copyright (c) 2013, Jens Deters
 * http://www.jensd.de
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package de.jensd.sweethome.core;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Jens Deters
 */
public class DeviceControl {

    private Send send = new Send();
    private static final Logger logger = Logger.getLogger(Configuration.class.getName());

    public String turnOn(Device device) {
        return doSwitch(device, Send.Command.TURN_ON);
    }

    public String turnOff(Device device) {
        return doSwitch(device, Send.Command.TURN_OFF);
    }

    public String doSwitch(Device device, Send.Command command) {
        logger.log(Level.INFO, "About to {0} {1} ({2})", new Object[]{command, device.getName(), device.getId()});
        if (send.isSendCommandExecutable()) {
            return send.send(device.getId(), command);
        } else {
            logger.log(Level.SEVERE, "{0} can not be executed.", command);
            return command + "can not be executed.";
        }
    }
}
