/**
 * Copyright (c) 2014, Jens Deters
 * http://www.jensd.de
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package de.jensd.sweethome.core;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author Jens Deters
 */
public class MqttClientConfiguration {

    private StringProperty mqttClientIdProperty;
    private StringProperty mqttBrokerAddressProperty;
    private StringProperty mqttBrokerPortProperty;
    private StringProperty mqttMessagesTopicProperty;

    public StringProperty mqttMessagesTopicProperty() {
        if(mqttMessagesTopicProperty == null){
            mqttMessagesTopicProperty = new SimpleStringProperty("sweethome");
        }
        return mqttMessagesTopicProperty;
    }

    
    
    public StringProperty mqttClientIdProperty() {
        if (mqttClientIdProperty == null) {
            mqttClientIdProperty = new SimpleStringProperty("MQTTClientId");
        }
        return mqttClientIdProperty;
    }

    public StringProperty mqttBrokerAddressProperty() {
        if (mqttBrokerPortProperty == null) {
            mqttBrokerAddressProperty = new SimpleStringProperty("127.0.0.1");
        }
        return mqttBrokerAddressProperty;
    }

    public StringProperty mqttBrokerPortProperty() {
        if (mqttBrokerPortProperty == null) {
            mqttBrokerPortProperty = new SimpleStringProperty("1883");
        }
        return mqttBrokerPortProperty;
    }

    public String getMqttMessagesTopic() {
        return mqttMessagesTopicProperty().get();
    }

    public void setMqttMessagesTopic(String mqttMessagesTopic) {
        mqttMessagesTopicProperty().set(mqttMessagesTopic);
    }

    
    
    public String getMqttClientId() {
        return mqttClientIdProperty().get();
    }

    public void setMqttClientId(String mqttClientId) {
        mqttClientIdProperty().set(mqttClientId);
    }

    public String getMqttBrokerAddress() {
        return mqttBrokerAddressProperty().get();
    }

    public String getMqttBrokerPort() {
        return mqttBrokerPortProperty().get();
    }

    public void setMqttBrokerAddress(String mqttBrokerAddress) {
        mqttBrokerAddressProperty().set(mqttBrokerAddress);
    }

    public void setMqttBrokerPort(String mqttBrokerPort) {
        mqttBrokerPortProperty().set(mqttBrokerPort);
    }

    public String getMqttBrokerURI() {
        return String.format("tcp://%s:%s", getMqttBrokerAddress(), getMqttBrokerPort());
    }
}
