/**
 * Copyright (c) 2013, Jens Deters http://www.jensd.de
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *
 */
package de.jensd.sweethome.core;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author Jens Deters
 *
 */
public class RESTDeviceControl extends DeviceControl {

    private static final Logger logger = Logger.getLogger(RESTDeviceControl.class.getName());
    private final Configuration configuration;

    public RESTDeviceControl(Configuration configuration) {
        this.configuration = configuration;
    }

    @Override
    public String doSwitch(Device device, Send.Command command) {
        logger.log(Level.INFO, "doSwitch: about to {0} {1} ({2})", new Object[]{command, device.getName(), device.getId()});
        return send(device.getHouseCode(), device.getGroupId(), device.getDeviceId(), String.valueOf(command.ordinal()));
    }

    public String send(String houseCode, String group, String device, String command) {
        String targetString = String.format("http://%s:%s", configuration.getServerName(), configuration.getHttpPort());
        String pathString = String.format("/intertechno/send/%s/%s/%s/%s", houseCode, group, device, command);
        String response = ClientBuilder
                .newClient()
                .target(targetString)
                .path(pathString)
                .request(MediaType.TEXT_PLAIN_TYPE)
                .get(String.class);
        logger.log(Level.INFO, "send: response={0}", response);
        return response;
    }
}
